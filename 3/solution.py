import timeit
def sort():

    start_time = timeit.default_timer()

    with open('sorted.txt', 'w') as f:
        for n in range(999999):
            f.write(str(n) + '\n')
    elapsed = timeit.default_timer() - start_time
    print(elapsed)


if __name__ == '__main__':
    sort()

